<?php

function zeige_aenderungen_lokal($database,$debug) {
  $db = new SQLite3('../data/'.$database);
  $qry = "SELECT * FROM tbltable WHERE fldaktiv='J'";
  $results = $db->query($qry);
  $arrdbtables = array();
  $arrdbprefix = array();
  $arrdbindex = array();
  $arrdbbez = array();
  $arriddb = array();
  $arrdbtyp = array();
  $arrdbname = array();
  $arrdbuser = array();
  $arrdbpassword = array();
  $anztables=0;
  while ($line = $results->fetchArray()) {
    $anztables=$anztables+1;
    array_push($arrdbtables,$line['flddbtblname']);
    array_push($arrdbprefix,$line['flddbtblprefix']);
    if ($line['flddbtblprefix']<>"") {
      array_push($arrdbindex,$line['flddbtblprefix'].$line['flddbtblindex']);
      array_push($arrdbbez,$line['flddbtblprefix'].$line['flddbtblbez']);
    } else {	
      array_push($arrdbindex,$line['flddbtblindex']);
      array_push($arrdbbez,$line['flddbtblbez']);
    }
    array_push($arriddb,$line['fldid_vondatabase']);
  }

  if ($anztables==0) {
    echo "<div class='alert alert-warning'>";
    echo "Keine Tables zum synchronisieren aktiviert.";
    echo "</div>";
  } else {
    for($tablecount = 0; $tablecount < $anztables; $tablecount++) {
      $dbqry = "SELECT * FROM tbldatabase WHERE fldindex=".$arriddb[$tablecount];
      if ($debug=="J") {
        echo "<div class='alert alert-info'>";
        echo $dbqry;
        echo "</div>";
      }
      $resdb = $db->query($dbqry);
      if ($lindb = $resdb->fetchArray()) {
        array_push($arrdbtyp,$lindb['flddbtyp']);
        array_push($arrdbname,$lindb['fldbez']);
        array_push($arrdbuser,$lindb['flddbuser']);
        array_push($arrdbpassword,$lindb['flddbpassword']);
      }
    }
    echo "<table class='table table-hover'>";
    echo "<tr><th>Table</th><th>timestamp</th><th>Index</th><th>Bezeichnung</th></tr>";
    for($tablecount = 0; $tablecount < $anztables; $tablecount++) {
      $query="SELECT * FROM tblsyncstatus WHERE fldtable='".$arrdbtables[$tablecount]."'";
      if ($debug=="J") {
        echo "<div class='alert alert-warning'>";
	     echo $query."<br>";
        echo "</dif>";
      }   
      $resst = $db->query($query);
      if ($linst = $resst->fetchArray()) {
	     $timestamp=$linst['fldtimestamp'];
	   } else {
        $timestamp='2015-01-01 00:00:00'; 
	   }
      if ($debug=="J") {
        echo "<div class='alert alert-info'>";
        echo $arrdbtyp[$tablecount].",".$arrdbname[$tablecount].",".$arrdbuser[$tablecount].",".$arrdbpassword[$tablecount];
        echo "</div>";
      }
      $dbopen=dbopentyp($arrdbtyp[$tablecount],$arrdbname[$tablecount],$arrdbuser[$tablecount],$arrdbpassword[$tablecount]);
      if ($arrdbprefix[$tablecount]<>"") {
        $qryval = "SELECT * FROM ".$arrdbtables[$tablecount]." WHERE ".$arrdbprefix[$tablecount]."dbsyncstatus='SYNC' AND ".$arrdbprefix[$tablecount]."dbtimestamp>'".$timestamp."'";
      } else {
        $qryval = "SELECT * FROM ".$arrdbtables[$tablecount]." WHERE flddbsyncstatus='SYNC' AND fldtimestamp>'".$timestamp."'";
      }  
      $resval = dbquerytyp($arrdbtyp[$tablecount],$dbopen,$qryval);
      while ($linval = dbfetchtyp($arrdbtyp[$tablecount],$resval)) {
        echo "<tr>";
        echo "<td>".$arrdbtables[$tablecount]."</td>";
        echo "<td>".$timestamp."</td>";
        echo "<td>".$linval[$arrdbindex[$tablecount]]."</td>";
        echo "<td>".utf8_encode($linval[$arrdbbez[$tablecount]])."</td>";
        echo "</tr>";
      }  
    }	
    echo "</table>";
  }
}

function update_nosync($database,$debug) {
  $db = new SQLite3('../data/'.$database);
  $qry = "SELECT * FROM tbltable WHERE fldaktiv='J'";
  $results = $db->query($qry);
  $arrdbtables = array();
  $arrdbprefix = array();
  $arrdbindex = array();
  $arrdbbez = array();
  $arriddb = array();
  $arrdbtyp = array();
  $arrdbname = array();
  $arrdbuser = array();
  $arrdbpassword = array();
  $anztables=0;
  while ($line = $results->fetchArray()) {
    $anztables=$anztables+1;
    array_push($arrdbtables,$line['flddbtblname']);
    array_push($arrdbprefix,$line['flddbtblprefix']);
    if ($line['flddbtblprefix']<>"") {
      array_push($arrdbindex,$line['flddbtblprefix'].$line['flddbtblindex']);
      array_push($arrdbbez,$line['flddbtblprefix'].$line['flddbtblbez']);
    } else {	
      array_push($arrdbindex,$line['flddbtblindex']);
      array_push($arrdbbez,$line['flddbtblbez']);
    }
    array_push($arriddb,$line['fldid_vondatabase']);
  }
  if ($anztables==0) {
    echo "<div class='alert alert-warning'>";
    echo "Keine Tables zum synchronisieren aktiviert.";
    echo "</div>";
  } else {
    for($tablecount = 0; $tablecount < $anztables; $tablecount++) {
      $dbqry = "SELECT * FROM tbldatabase WHERE fldindex=".$arriddb[$tablecount];
      if ($debug=="J") {
        echo "<div class='alert alert-info'>";
        echo $dbqry;
        echo "</div>";
      }
      $resdb = $db->query($dbqry);
      if ($lindb = $resdb->fetchArray()) {
        array_push($arrdbtyp,$lindb['flddbtyp']);
        array_push($arrdbname,$lindb['fldbez']);
        array_push($arrdbuser,$lindb['flddbuser']);
        array_push($arrdbpassword,$lindb['flddbpassword']);
      }
    }
    for($tablecount = 0; $tablecount < $anztables; $tablecount++) {
      $query="SELECT * FROM tblsyncstatus WHERE fldtable='".$arrdbtables[$tablecount]."'";
      if ($debug=="J") {
        echo "<div class='alert alert-warning'>";
	     echo $query."<br>";
        echo "</dif>";
      }   
      $resst = $db->query($query);
      if ($linst = $resst->fetchArray()) {
	     $timestamp=$linst['fldtimestamp'];
	   } else {
        $timestamp='2015-01-01 00:00:00'; 
	   }
      if ($debug=="J") {
        echo "<div class='alert alert-info'>";
        echo $arrdbtyp[$tablecount].",".$arrdbname[$tablecount].",".$arrdbuser[$tablecount].",".$arrdbpassword[$tablecount];
        echo "</div>";
      }
      $dbopen=dbopentyp($arrdbtyp[$tablecount],$arrdbname[$tablecount],$arrdbuser[$tablecount],$arrdbpassword[$tablecount]);
      if ($arrdbprefix[$tablecount]<>"") {
        $qryval = "UPDATE ".$arrdbtables[$tablecount]." SET ".$arrdbprefix[$tablecount]."dbsyncstatus='NOSYNC' WHERE ".$arrdbprefix[$tablecount]."dbsyncstatus='SYNC' AND ".$arrdbprefix[$tablecount]."dbtimestamp>'".$timestamp."'";
      } else {
        $qryval = "UPDATE ".$arrdbtables[$tablecount]." SET flddbsyncstatus='NOSYNC' WHERE flddbsyncstatus='SYNC' and fldtimestamp>'".$timestamp."'";
      }
      if ($debug=="J") {
        echo "<div class='alert alert-info'>";
        echo $qryval."<br>";  
        echo "</div>";
      }
      dbexecutetyp($arrdbtyp[$tablecount],$dbopen,$qryval);
    }    
    echo "<div class='alert alert-info'>";
    echo "Nosync fertig.";
    echo "</div>";   
  }  
}

?>