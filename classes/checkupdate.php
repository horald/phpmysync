<?php
include("bootstrapfunc.php");
include("../config.php");
bootstraphead("");
bootstrapbegin("PhpMySync - Checkupdate<br>","");

echo "<a href='../index.php' class='btn btn-primary btn-sm active' role='button'>Menü</a> "; 
echo "<a href='about.php' class='btn btn-primary btn-sm active' role='button'>zurück</a> "; 

$string = file_get_contents("../version.json");
$json_a = json_decode($string, true);
$versnr=$json_a['versnr'];
$versdat=$json_a['versdat'];

$string = file_get_contents("https://gitlab.com/horald/phpmysync/raw/master/version.json");
$json_a = json_decode($string, true);
$remoteversnr=$json_a['versnr'];
$remoteversdat=$json_a['versdat'];

echo "<pre>";
echo "<table>";
echo "<tr><td>lokaler Stand</td>  <td>: ".$versdat."</td></tr>";
echo "<tr><td>lokale Version</td><td>: ".$versnr."</td></tr>";
echo "<tr><td>Neuer Stand</td>  <td>: ".$remoteversdat."</td></tr>";
echo "<tr><td>Neue Version</td><td>: ".$remoteversnr."</td></tr>";
echo "</table>";
echo "</pre>";

if ($remoteversnr>$versnr) {
  echo "<form name='eingabe' class='form-horizontal' method='post' action='finishupdate.php'>";
  echo "     <button type='submit' name='submit' value='save' class='btn btn-primary'>Update einlesen</button>";
  echo "<input type='hidden' name='versnr' value='".$versnr."'>";
  echo "<input type='hidden' name='versdat' value='".$versdat."'>";
  echo "</form>";
} else {
  echo "<div class='alert alert-success'>";
  echo "Ihre Version ist auf dem aktuellen Stand.";
  echo "</div>";
}

bootstrapend();
?>