<?php
header("content-type: text/html; charset=utf-8");

function deleteinput($pararray,$idwert,$menu,$user,$callback) {
  $db = new SQLite3('../data/joorgsqlite.db');
  $dbtable=$pararray['dbtable'];
  $bez="";
  if ($pararray['fldbez']!="") {
  	$bez=$pararray['fldbez']; 
  } else {
  	$bez="dummy";
  }	   
  $sql="SELECT * FROM ".$dbtable." WHERE ".$pararray['fldindex']."=".$idwert; 
  $results = $db->query($sql);
  $line = $results->fetchArray();
  if ($bez=="dummy") {
    $bez=$line[1];
  } else {	
    $bez=$line[$pararray['fldbez']];
  }    	
  if ($callback=="calendar") {
    echo "<a href='calendar.php' class='btn btn-primary btn-sm active' role='button'>Zurück</a>";
  } else {
    echo "<a href='showtab.php?menu=".$menu."&user=".$user."' class='btn btn-primary btn-sm active' role='button'>Zurück</a>";
  }   
  echo "<form class='form-horizontal' method='post' action='delete.php?delete=1&menu=".$menu."&id=".$idwert."&user=".$user."&callback=".$callback."'>";

  echo "<dl>";
  echo "<dt>'".$bez."' wirklich löschen?</dt>";
  echo "</dl>";

  echo "<input type='hidden' name='fldbez' value='".$bez."'>";
  echo "<dd><input type='submit' value='Löschen' /></dd>";
  echo "</form>";
}

function deletesave($pararray,$idwert,$fldbez,$menu) {
  echo "<a href='showtab.php?menu=".$menu."' class='btn btn-primary btn-sm active' role='button'>Liste</a>"; 
  $db = new SQLite3('../data/joorgsqlite.db');
//echo $idwert."=idwert<br>";
//  echo $db->lastErrorMsg()."<br>";
  $dbtable=$pararray['dbtable'];
  if ($pararray['dellogical']=="J") {
  	 $sql="UPDATE ".$dbtable." SET flddel='J' WHERE fldindex=".$idwert;
  } else {
    $sql="DELETE FROM ".$dbtable." WHERE fldindex=".$idwert;
  }  
  $db->exec($sql);
//  echo $db->lastErrorMsg()."<br>";
  echo "<div class='alert alert-success'>";
  echo "Daten '".$fldbez."' gelöscht.";
  echo "</div>";
}

?>