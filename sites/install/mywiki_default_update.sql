ALTER TABLE archive MODIFY COLUMN ar_dbsyncnr int(11) DEFAULT 3;
ALTER TABLE category MODIFY COLUMN cat_dbsyncnr int(11) DEFAULT 3;
UPDATE category SET cat_dbsyncnr=3;
  
ALTER TABLE category
  ADD cat_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD cat_dbsyncnr  int(11) DEFAULT 9,
  ADD cat_dbsyncstatus varchar(10) DEFAULT 'SYNC';
  
ALTER TABLE categorylinks
  ADD cl_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD cl_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD cl_dbsyncnr  int(11) DEFAULT 9,
  ADD cl_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD PRIMARY KEY(cl_index);
  
ALTER TABLE externallinks
  ADD el_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD el_dbsyncnr  int(11) DEFAULT 9,
  ADD el_dbsyncstatus varchar(10) DEFAULT 'SYNC';

ALTER TABLE interwiki  
  ADD iw_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD iw_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD iw_dbsyncnr  int(11) DEFAULT 9,
  ADD iw_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD PRIMARY KEY(iw_index);

ALTER TABLE l10n_cache  
  ADD lc_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD lc_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD lc_dbsyncnr  int(11) DEFAULT 9,
  ADD lc_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD PRIMARY KEY(lc_index);

ALTER TABLE logging
  ADD log_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD log_dbsyncnr  int(11) DEFAULT 9,
  ADD log_dbsyncstatus varchar(10) DEFAULT 'SYNC';

ALTER TABLE module_deps
  ADD md_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD md_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD md_dbsyncnr  int(11) DEFAULT 9,
  ADD md_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD PRIMARY KEY(md_index);
  
ALTER TABLE msg_resource
  ADD mr_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD mr_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD mr_dbsyncnr  int(11) DEFAULT 9,
  ADD mr_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD PRIMARY KEY(mr_index);

ALTER TABLE msg_resource_links
  ADD mrl_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD mrl_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD mrl_dbsyncnr  int(11) DEFAULT 9,
  ADD mrl_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD PRIMARY KEY(mrl_index);

ALTER TABLE objectcache
  ADD oc_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD oc_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD oc_dbsyncnr  int(11) DEFAULT 9,
  ADD oc_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD INDEX(oc_index);

ALTER TABLE page
  ADD page_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD page_dbsyncnr  int(11) DEFAULT 9,
  ADD page_dbsyncstatus varchar(10) DEFAULT 'SYNC';
  
ALTER TABLE pagelinks
  ADD pl_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD pl_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD pl_dbsyncnr  int(11) DEFAULT 9,
  ADD pl_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD INDEX(pl_index);
  
ALTER TABLE recentchanges
  ADD rc_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD rc_dbsyncnr  int(11) DEFAULT 9,
  ADD rc_dbsyncstatus varchar(10) DEFAULT 'SYNC';

ALTER TABLE revision
  ADD rev_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD rev_dbsyncnr  int(11) DEFAULT 9,
  ADD rev_dbsyncstatus varchar(10) DEFAULT 'SYNC';

ALTER TABLE searchindex
  ADD si_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD si_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD si_dbsyncnr  int(11) DEFAULT 9,
  ADD si_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD INDEX(si_index);
  
ALTER TABLE site_stats
  ADD ss_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD ss_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD ss_dbsyncnr  int(11) DEFAULT 9,
  ADD ss_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD INDEX(ss_index);
  
ALTER TABLE templatelinks
  ADD tl_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD tl_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD tl_dbsyncnr  int(11) DEFAULT 9,
  ADD tl_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD INDEX(tl_index);

ALTER TABLE text
  ADD old_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD old_dbsyncnr  int(11) DEFAULT 9,
  ADD old_dbsyncstatus varchar(10) DEFAULT 'SYNC';
  
ALTER TABLE watchlist
  ADD wl_index bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  ADD wl_dbtimestamp timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ADD wl_dbsyncnr  int(11) DEFAULT 9,
  ADD wl_dbsyncstatus varchar(10) DEFAULT 'SYNC',
  ADD INDEX(wl_index);
